require 'capybara/poltergeist'
require 'capybara/dsl'       
require "capybara-webkit"
require 'mechanize-random-agent'

class PoltergeistCrawler
  include Capybara::DSL 

  def initialize
    Capybara.register_driver :poltergeist_crawler do |app| 
      Capybara::Poltergeist::Driver.new(app, {
        # phantomjs_options: ["--web-security=false"],#--proxy=127.0.0.1:9050", "--proxy-type=socks5"],
        # phantomjs_logger:  open('/home/user/sandbox/parsers/logs/phantom.log', 'w'),#/dev/null'),
          #logger:  open('/home/user/sandbox/parsers/logs/phantom.log', 'w'),#/dev/null'),
        js_errors:         true,                  
        inspector:         false, 
        timeout:           60,
        debug:             true,
        window_size:       [800, 600]
      })
    end 
    Capybara.javascript_driver = :poltergeist
    #Capybara.javascript_driver = :webkitheadless
    Capybara.default_max_wait_time = 60
    Capybara.run_server = false       
    Capybara.default_driver = :poltergeist_crawler
    m = Mechanize.new
    page.driver.headers = {                       
      "User-Agent" => m.get_random_user_agent
    }
    page.driver.browser.url_blacklist = [
      'https://www.google-analytics.com/'
    ]
  end

  def screenshot(path)
    page.driver.render("#{path}.jpg",full: true)
  end
  
  def doc                    
    Nokogiri.parse(page.body)
  end 
end