require 'google_drive'
require 'mail'

mails_base = 'crossout'
mails_folder = './mails/'

p 'Starting'

mails = []
Dir[mails_folder+"2016*"].each do |file|
  mail = Mail.read(file)
  mails << mail.to.to_s.gsub(/[\[\]\"]/, '')
end

session = GoogleDrive.saved_session("config.json")
ws = session.file_by_title(mails_base).worksheets[0]

p 'Getting mails from google drive'

passwords = {}
(1..ws.num_rows).each do |row|

  if mails.include? ws[row,1]
    passwords[ws[row,1]] = ws[row,3]
  end

end

session = GoogleDrive.saved_session("config.json")
ws = session.file_by_title(mails_base).worksheets[1]

i=ws.num_rows
passwords.sort.each do |mail|
  i+=1
  ws[i,1]=mail[0]
  ws[i,2]=mail[1]
  ws[i,3]=Time.now
end

ws.save