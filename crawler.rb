require '/home/user/work/sites/parser_ruby/rucaptcha/rucaptcha_api'
require 'capybara/poltergeist'
require 'capybara/dsl'
require 'open-uri'

system_call_newnim = "$PWD/newnim.sh"

class PoltergeistCrawler
  include Capybara::DSL 

  def initialize
    Capybara.register_driver :poltergeist_crawler do |app| 
      Capybara::Poltergeist::Driver.new(app, {
        :js_errors => false,                  
        :inspector => false, 
        timeout: 30,
        window_size: [1024, 768],
        :phantomjs_options => ["--proxy=127.0.0.1:8118"],
        phantomjs_logger: open('/dev/null') # if you don't care about JS errors/console.logs
      })
    end 
#    Capybara.reset_sessions!
    Capybara.default_max_wait_time = 20
    Capybara.run_server = false       
    Capybara.default_driver = :poltergeist_crawler
#    page.driver.reset!
    page.driver.headers = {                       
      "DNT" => 1,                                 
      "User-Agent" => "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0"
    }
    page.driver.browser.url_blacklist = [
      'http://vk.com'
    ]
  end

  # handy to peek into what the browser is doing right now
  def screenshot(name="screenshot")
    page.driver.render("/home/user/Downloads/#{name}.jpg",full: true)
  end
  
  # find("path") and all("path") work ok for most cases. Sometimes I need more control, like finding hidden fields
  def doc                    
    Nokogiri.parse(page.body)
  end 
end

class ExampleCrawler < PoltergeistCrawler

  def fill (css, value)
    element = find(css)
    element.native.send_key(value)
    sleep 0.5
  end

  def get_captcha(captcha_image_file)
    api = RucaptchaApi.new '98762331c2b25bc7122e04482bf34156'
    path_to_captcha = File.expand_path captcha_image_file
    captcha_id = api.send_captcha_for_solving path_to_captcha, params: {phrase: 0}
    solved_captcha = api.get_solved_captcha captcha_id 
  end

  def crawl
    login_page = 'https://passport.yandex.ru/registration/mail?from=mail&origin=home_v14_ru&retpath=https%3A%2F%2Fmail.yandex.ru'
    visit login_page
    sleep 1
    screenshot('callbacks1')
    #click_on "У меня нет телефона"
    find(".human-confirmation-via-captcha").click
    screenshot('callbacks2')
    sleep 1

    captcha_uri = ''
    d = doc.css("img.captcha__captcha__text")
    dd = d.first
    dd.each do |r|
      captcha_uri = r[1] if r[0]=='src'
    end

    image_filename = captcha_uri.gsub(/\D/, "")
    rand = Random.rand(9999999)
    captcha_image_filename = "#{__dir__}/captches/#{image_filename}_#{rand}.gif"
    File.open(captcha_image_filename,'wb'){ |f| f.write(open(captcha_uri).read) }
    captcha_answer = get_captcha(captcha_image_filename)


    fill('#firstname', 'Агнисий')
    begin
      fill('#surname', 'Изуев')
    rescue
      fill('#lastname', 'Изуев')
    end
    fill('#login', 'agnisii.izuev1977')
    fill('#password', 'ghb4elf')
    fill('#password_confirm', 'ghb4elf')
    fill('#answer', captcha_answer.force_encoding('utf-8'))
#    fill('#hint_question_id', '3')
    fill('#hint_answer', '620000')

    page.execute_script %Q{
      $('#hint_question_id').trigger('keydown').val('3').trigger('keyup')
    }

    sleep(0.3)

    page.execute_script %Q{
      $('#ui-id-1 li:nth-last-child(2) a').click()
    }



    screenshot('callbacks3')



    element = find('#nb-5').click
    #element.native.send_key(:Enter)
    sleep 2

    screenshot('callbacks4')

    element = find('.b-popup__close').click

    sleep 2

    screenshot('callbacks5')

    # find("#text").click
#     element = find('#text')
#     element.native.send_key('String')
#     sleep 1
#     screenshot('callbacks2')
#     element.native.send_key(:Enter)
# #    find('.search2__button').click
#     sleep 5
#     screenshot('callbacks3')
    page.driver.quit
#fill_in 'Email', with: author.email
    # page.evaluate_script("window.location = '/'")
  end
end

system(system_call_newnim)
sleep 1

ExampleCrawler.new.crawl