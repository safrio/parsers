require 'translit'

class FishGenerator

  @@surnames = '/home/user/sandbox/_bases/surnames.txt'
  @@male_names = '/home/user/sandbox/_bases/males.txt'
  @@female_names = '/home/user/sandbox/_bases/females.txt'

  @@logins_dir = '/home/user/sandbox/_bases/logins/logins'
  @@logins_ext = '.txt'
  @@login_files_count = 937
  @@logins_count_per_file = 100000

  def initialize(gender='random')
    case gender
      when 'female'
        female
      when 'male'
        male
     else
        if Random.rand(100) < 30
          male
        else
          female
        end
    end
  end

  def random_line_from(file)
    IO.readlines(file)[Random.rand(open(file).read.count("\n")-1)].strip
  end

  def male
    @name     = random_line_from(@@male_names)
    @surname  = random_line_from(@@surnames).gsub(/а$/, '') 
    @login    = get_login
    @password = get_password
  end

  def female
    @name     = random_line_from(@@female_names)
    @surname  = random_line_from(@@surnames).gsub(/а$/, 'а')
    @login    = get_login
    @password = get_password
  end

  def name
    @name
  end

  def surname
    @surname
  end

  def login(length=28)
    @login[0..length].gsub(/^\W+/, '').gsub(/\W+$/, '')
  end

  def password
    @password
  end

  def to_s
    @name + ' ' + @surname
  end

  def translit
    @name     = Translit.convert(@name)
    @surname  = Translit.convert(@surname)
    self
  end

  def get_login
    @login = ''
    2.times do
      @login += IO.readlines(@@logins_dir+(Random.rand(@@login_files_count)+1).to_s+@@logins_ext)[Random.rand(@@logins_count_per_file)].strip
    end
    @login = @login.downcase.gsub(/\_+/, '.')
  end

  def get_password
    @password = ''
    2.times do
      @password += IO.readlines(@@logins_dir+(Random.rand(@@login_files_count)+1).to_s+@@logins_ext)[Random.rand(@@logins_count_per_file)].strip
    end
    @password
  end

end

#EXAMPLE:
# 120.times do
#   fish = FishGenerator.new('female')
#   # p fish.name + ' ' + fish.surname
#   # fish.translit
#   # p (fish.name + ' ' + fish.surname).downcase
#   p fish.login + ' ' + fish.login.to_s.length.to_s
#   # p fish.password
# end
