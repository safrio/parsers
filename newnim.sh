#!/bin/sh
# Автоматическая смена НИМа в TOR
# empty -f -i torin -o torout telnet 127.0.0.1 9051
# empty -s -o torin "AUTHENTICATE \"s6afyhe6eu\"\n"
# empty -s -o torin "SETEVENTS SIGNAL\n"
# empty -s -o torin "SIGNAL NEWNYM\n"
# empty -s -o torin "quit\n"
echo 1 | sudo -S -p 1 service tor restart