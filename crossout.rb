require '/home/user/work/sites/parser_ruby/rucaptcha/rucaptcha_api'
require '/home/user/work/sites/parser_ruby/fish_generator'
require 'mechanize-random-agent'
require 'google_drive'
require 'capybara/poltergeist'
require 'capybara/dsl'       
require 'open-uri'

class PoltergeistCrawler
  include Capybara::DSL 

  def initialize
    Capybara.register_driver :poltergeist_crawler do |app| 
      Capybara::Poltergeist::Driver.new(app, {
        :js_errors => false,                  
        :inspector => false, 
        timeout: 30,
        window_size: [1024, 768],
        :phantomjs_options => ["--proxy=127.0.0.1:9050", "--proxy-type=socks5"],
        phantomjs_logger: open('/dev/null') # if you don't care about JS errors/console.logs
      })
    end 
    Capybara.default_max_wait_time = 20
    Capybara.run_server = false       
    Capybara.default_driver = :poltergeist_crawler
    m = Mechanize.new
    page.driver.headers = {                       
      "User-Agent" => m.get_random_user_agent
    }
    page.driver.browser.url_blacklist = [
      'http://crossout.net/i/bg-main.jpg'
    ]
  end

  def screenshot(name="screenshot")
    page.driver.render("/home/user/work/sites/parser_ruby/screenshots2/#{name}.jpg",full: true)
  end
  
  def doc                    
    Nokogiri.parse(page.body)
  end 
end

class ExampleCrawler < PoltergeistCrawler

  @@system_call_newnim = "$PWD/newnim.sh"

  @@google_drive_base = 'yandex'
  @@google_drive_out  = 'crossout'
  @@n=1
  @@done_list_max_row = 0

  @@mail_domains = [
    '@narod.ru',
    '@ya.ru',
    '@yandex.by',
    '@yandex.com',
    '@yandex.kz',
    '@yandex.ru',
    '@yandex.ua'
  ]

  def fill (css, value)
    element = find(css)
    element.native.send_key(value)
    sleep 0.05
  end

  def get_captcha(captcha_image_file)
    api = RucaptchaApi.new '98762331c2b25bc7122e04482bf34156'
    path_to_captcha = File.expand_path captcha_image_file
    captcha_id = api.send_captcha_for_solving path_to_captcha, params: {phrase: 0}
    begin
      solved_captcha = api.get_solved_captcha captcha_id 
    rescue
      p "Can't GET CAPTCHA"
      return
    end
  end

  def crawl

    all_mails = []
    done_mails = []
p 1
    begin
      session = GoogleDrive.saved_session("config.json")
      ws = session.file_by_title(@@google_drive_base).worksheets[1]
      (1..ws.num_rows).each do |row|
        if ws[row,7] != 'skiped'
          @@mail_domains.map { |domain| all_mails.push ws[row,1]+domain }
        end
      end
p 2
      session = GoogleDrive.saved_session("config.json")
      ws = session.file_by_title(@@google_drive_out).worksheets[0]
      @@done_list_max_row = ws.num_rows
      (1..ws.num_rows).each do |row|
        done_mails.push ws[row,1]
      end
p 3
    rescue
      p "Can't reach GOOGLE DRIVE!"
      return
    end
p 4
    to_crawl = all_mails - done_mails

    p 'To crawl list generated: '+to_crawl.length.to_s

    to_crawl.shuffle.each do |email|

      system(@@system_call_newnim)
      sleep 1.5


      fish = FishGenerator.new()
      p email
      # p fish.login(15).gsub(/\W/, '')
      # p fish.password

      login_page = 'http://crossout.net/ru/play'
      begin
        visit login_page
      rescue
        p "Can't REACH LOGIN SERVER"
        return
      end

      p 'Login page visited'

      sleep 1
      screenshot("#{email}_1")

      captcha_uri = ''
      d = doc.css("img#captcha")
      dd = d.first
      dd.each do |r|
        captcha_uri = r[1] if r[0]=='src'
      end


      captcha_image_filename = "/home/user/work/sites/parser_ruby/screenshots2/#{email}_captcha.jpg"
      page.driver.render(captcha_image_filename, :selector => '.captcha')

      system("convert -crop 110x30+275+20 #{captcha_image_filename} #{captcha_image_filename}")

      sleep 0.4

      captcha_answer = ''

      begin
        captcha_answer = get_captcha(captcha_image_filename)
        p 'Captcha answer is: ' + captcha_answer.force_encoding('utf-8')
      rescue
        p "Can't GET CAPTCHA ANSWER"
        return
      end




      fill('#nick', fish.login(15).gsub(/\W/, ''))
      fill('#login', email)
      fill('#password', fish.password)
      fill('#passwordrepeat', fish.password)

      begin
        page.execute_script %Q{
          $('#captcha').val('#{captcha_answer}')
        }
      rescue
        p "Can't find CAPTCHA INPUT"
      end

      screenshot("#{email}_2")
      sleep 0.3

      element = find('.js-registration').click
      sleep 4
      screenshot("#{email}_3")


      begin
        captcha_uri2 = ''
        d = doc.css("img#captcha")
        dd = d.first
        dd.each do |r|
          captcha_uri2 = r[1] if r[0]=='src'
        end

        p 'Captcha is WRONG!' if captcha_uri2 == captcha_uri
      rescue

        begin
          @@done_list_max_row = @@done_list_max_row + 1
          session = GoogleDrive.saved_session("config.json")
          ws = session.file_by_title(@@google_drive_out).worksheets[0]
          ws[@@done_list_max_row, 1] = email
          ws[@@done_list_max_row, 2] = fish.login(15).gsub(/\W/, '')
          ws[@@done_list_max_row, 3] = fish.password
          ws.save
        rescue
          p "Can't reach GOOGLE DRIVE!"
          return
        end


        p "Done!"
      end

      Capybara.reset_sessions!

    end
  end
end

while (1)
ExampleCrawler.new.crawl
ExampleCrawler.class_variable_set(:@@n, ExampleCrawler.class_variable_get(:@@n)+1)
end