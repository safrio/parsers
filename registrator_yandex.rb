require '/home/user/sandbox/parsers/fish_generator'
require "/home/user/sandbox/parsers/rucaptcha/rucaptcha_api"
require 'mechanize-random-agent'
require 'google_drive'
require 'capybara/poltergeist'
require 'capybara/dsl'       
require 'open-uri'

system_call_newnim = "$PWD/newnim.sh"

class PoltergeistCrawler
  include Capybara::DSL 

  def initialize
    Capybara.register_driver :poltergeist_crawler do |app| 
      Capybara::Poltergeist::Driver.new(app, {
        js_errors: false,                  
        inspector: false, 
        timeout: 30,
        window_size: [1024, 768],
        phantomjs_options: ["--proxy=127.0.0.1:9050", "--proxy-type=socks5"],
        phantomjs_logger: open('/dev/null') # if you don't care about JS errors/console.logs
      })
    end 
    Capybara.default_max_wait_time = 20
    Capybara.run_server = false       
    Capybara.default_driver = :poltergeist_crawler
    m = Mechanize.new
    page.driver.headers = {                       
      "User-Agent" => m.get_random_user_agent
    }
    page.driver.browser.url_blacklist = [
      # 'http://vk.com'
    ]
  end

  # handy to peek into what the browser is doing right now
  def screenshot(name="screenshot")
    # page.driver.render("#{__dir__}/screenshots/#{name}.jpg",full: true)
  end
  
  # find("path") and all("path") work ok for most cases. Sometimes I need more control, like finding hidden fields
  def doc                    
    Nokogiri.parse(page.body)
  end 
end

class ExampleCrawler < PoltergeistCrawler

  @@google_drive_title = 'yandex'
  @@n=1

  def fill (css, value)
    element = find(css)
    element.native.send_key(value)
    sleep 0.05
  end

  def get_captcha(captcha_image_file)
    api = RucaptchaApi.new '98762331c2b25bc7122e04482bf34156'
    path_to_captcha = File.expand_path captcha_image_file
    captcha_id = api.send_captcha_for_solving path_to_captcha, params: {phrase: 0, language: 1, regsense: 1}
    begin
      solved_captcha = api.get_solved_captcha captcha_id 
    rescue
      p "Can't GET CAPTCHA"
      return
    end
  end

  def crawl

    fish = FishGenerator.new()
    p fish.name + ' ' + fish.surname
#    p fish.login(28)
#    p fish.password

    login_page = 'https://passport.yandex.ru/registration/mail?from=mail&origin=home_v14_ru&retpath=https%3A%2F%2Fmail.yandex.ru'
    begin
      visit login_page
    rescue
      p "Can't REACH LOGIN SERVER"
      return
    end

    p 'Login page visited'

    sleep 1
    screenshot(@@n.to_s + '_callbacks1')
    begin
      find(".human-confirmation-via-captcha").click
      p 'Captcha confirmation clicked'
    rescue
      p "Can't find confirmation link"
    end

    screenshot(@@n.to_s + '_callbacks2')
    sleep 0.3

#############------- CAPTCHA ---------#################

    captcha_image_filename = "#{__dir__}/screenshots/#{@@n}_captcha.jpg"
    page.driver.render(captcha_image_filename, :selector => '.captcha__captcha__text')

    captcha_answer = get_captcha(captcha_image_filename)

    begin
      p 'Captcha answer is: '
      p captcha_answer.force_encoding('utf-8')
    rescue
      p "Can't REACH CAPTCHA LINK"
      return
    end

#############------- CAPTCHA ---------#################

#    fill('#login', fish.login(28))

    fill('#firstname', fish.name)
    begin
      fill('#surname', fish.surname)
    rescue
      fill('#lastname', fish.surname)
    end

    element = find('#login')
    element.native.send_key(:Left)

    sleep 4

    page.execute_script "$('.login__suggestedLogin')[0].click()"

    sleep 5

    password = fish.password

    page.execute_script "$('#password').val('#{password}')"
    sleep 0.5
    page.execute_script "$('#password_confirm').val('#{password}')"
    sleep 0.5


    # fill('#password', password)
    # fill('#password_confirm', password)


    fill('#answer', captcha_answer.force_encoding('utf-8'))

    login = find('input#login')[:value]


    fill('#hint_answer', '620000')

    page.execute_script %Q{
      $('#hint_question_id').trigger('keydown').val('3').trigger('keyup')
    }
    sleep(0.3)
    page.execute_script %Q{
      $('#ui-id-1 li:nth-last-child(2) a').click()
    }


    screenshot(@@n.to_s + '_callbacks3')

    p 'Filling form complete'

    begin
      element = find('#nb-5').click
    rescue
      p "Can't find submit button!"
      return
    end

    p 'Pressing submit form'


    sleep 4

    # CAPTCHA IS WRONG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    begin
      unless find(".control__error__captcha_incorrect").nil?
        p 'WRONG F..G CAPTCHA!' 
        return
      end
    rescue
    end


    screenshot(@@n.to_s + '_callbacks4')

    ### WRONG FORM FILLNG!
    begin
      p find('#nb-5')
      p 'WRONG FORM FILLNG!'
      return
    rescue
    end


    begin
      session = GoogleDrive.saved_session("config.json")
      ws = session.file_by_title(@@google_drive_title).worksheets[2]
      row=ws.num_rows+1
      ws[row, 1] = login
      ws[row, 2] = password
      ws[row, 3] = fish.name
      ws[row, 4] = fish.surname
      ws[row, 5] = '3'
      ws[row, 6] = '620000'
      ws[row, 7] = Time.now
      ws.save
    rescue
      p "Can't reach GOOGLE DRIVE!"
      return
    end

    begin
      element = find('.b-popup__close').click
      sleep 0.15
      page.execute_script %Q{
        $('.b-popup__close').click()
      }
    rescue

      p "Can't press popup close"
      screenshot(@@n.to_s + '_callbacks5')
      Capybara.reset_sessions!
      return
    end

    p 'Pressing close popup'

    sleep 1

    screenshot(@@n.to_s + '_callbacks5')

    p 'Done'

    Capybara.reset_sessions!
  end
end

#100.times do
  system(system_call_newnim)
  sleep 2.5
  # p ExampleCrawler.class_variable_get(:@@n)
  system('torify curl icanhazip.com')

  begin  
    ExampleCrawler.new.crawl
    ExampleCrawler.class_variable_set(:@@n, ExampleCrawler.class_variable_get(:@@n).to_i+1)
  rescue
    p 'Something wrong with Poltergeist!!!'
#    next
  end
#end