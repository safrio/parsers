require 'net/imap'
#require 'mail'

mails_folder = './mails/'
folder = "INBOX"

imap = Net::IMAP.new('imap.yandex.ru', 993, true)
imap.login(ARGV[0], ARGV[1])
imap.select(folder)

imap.search(["UNSEEN", "NOT", "FROM", "yandex"]).each do |msg_id| #
  msg = imap.fetch(msg_id, "(UID RFC822.SIZE ENVELOPE BODY[TEXT])")[0]
  envelope = msg.attr["ENVELOPE"]
#  body_mail = msg.attr["BODY[TEXT]"]
  
  raw = imap.fetch(msg_id,'RFC822')[0].attr['RFC822']
  message_file = mails_folder+ARGV[0]+'_'+envelope.message_id.gsub(/(?:^\<)|(?:\>$)/,'')
  File.open(message_file,'wb'){ |f| f.write(raw) }

#  mail = Mail.read(message_file)

end

imap.logout
imap.disconnect
