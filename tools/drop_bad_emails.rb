require 'google_drive'
require 'yaml'

google_drive_base = 'yandex'
good_emails_file = 'yandex.norm'

good_emails = {}

_CONFIG = YAML.load_file('/home/user/sandbox/parsers/config.yml')

IO.readlines(good_emails_file).each do |line|
  good_emails[line.strip]=1
end

i=0
session = GoogleDrive.saved_session(_CONFIG['config_json'])
ws = session.file_by_title(google_drive_base).worksheets[2]
ws2 = session.file_by_title(google_drive_base).worksheets[3]
(1..ws.num_rows).each do |row|
  if !(ws[row,1].empty?) and good_emails[ws[row,1]] == 1
    i += 1
    ws2[i,1]=ws[row,1]
    ws2[i,2]=ws[row,2]
    ws2[i,3]=ws[row,3]
    ws2[i,4]=ws[row,4]
    ws2[i,5]=ws[row,5]
    ws2[i,6]=ws[row,6]
  end
end

ws2.save