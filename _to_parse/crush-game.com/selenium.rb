require 'capybara/dsl'
require 'selenium-webdriver'
require 'headless'
require 'mechanize-random-agent'

require 'google_drive'
require 'yaml'

class SeleniumCrawler
  include Capybara::DSL

  def initialize

    Capybara.run_server = false
    Capybara.current_driver = :selenium
    Capybara.app_host = "http://www.crush-game.com/"
    Capybara.default_max_wait_time = 6


    @@headless = Headless.new
    at_exit do
      @@headless.destroy
    end
   @@headless.start if Capybara.current_driver == :selenium



    _proxy = "socks5://localhost:9050"

    proxy = Selenium::WebDriver::Proxy.new(
      :http     => _proxy,
      :ftp      => _proxy,
      :ssl      => _proxy
    )


# caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => [ "--disable-web-security" ]})
# driver = Selenium::WebDriver.for :remote, url: 'http://localhost:4444/wd/hub' desired_capabilities: caps

    caps = Selenium::WebDriver::Remote::Capabilities.ie(:proxy => proxy) 
    m = Mechanize.new
    _user_agent = m.get_random_user_agent
     _user_data_dir = '~/.config/google-chrome/Default'#/mnt/c/Users/Akacki/AppData/Local/Google/Chrome/User Data/'
      
    @@driver = Selenium::WebDriver.for :chrome, :desired_capabilities => caps, :switches => %W[--user-agent=#{_user_agent} --user-data-dir=#{_user_data_dir}]

# caps = Selenium::WebDriver::Remote::Capabilities.chrome({
#       'chromeOptions' => {
#        'proxy-server' => '127.0.0.1:9050'#,
        # 'extensions' => [
        #   Base64.strict_encode64(File.open('C:\\App\\extension.crx', 'rb').read)
        # ]
    #   }
    # })

# profile = Selenium::WebDriver::Chrome::Profile.new 
# profile["intl.accept_languages"] = "de"

# caps = Selenium::WebDriver::Remote::Capabilities.chrome(
#   platform: "Linux", 
#   version: "", 
#   'chrome.profile' => profile.as_json['zip']
# )


#     @@driver = Selenium::WebDriver.for :chrome,   :desired_capabilities => caps, :switches => %W[--user-agent=#{_user_agent}]


    # Selenium::WebDriver.configure do |config|
    #   config.before :each, :js, type: :feature do |example|
#Capybara.current_driver.browser.url_blacklist = ["http://cms.staging.playmxm.com/uploads/mxm/"]
    #   end
    # end

  end

end

class ExampleCrawler < SeleniumCrawler

  @@system_call_newnim = "$PWD/newnim.sh"
  @@config_json = "/home/user/sandbox/parsers/config.json"

  @@google_drive_base = 'yandex'
  @@google_drive_out  = 'crush-game'


  @@mail_domains = [
    '@narod.ru',
    '@ya.ru',
    '@yandex.by',
    '@yandex.com',
    '@yandex.kz',
    '@yandex.ru',
    '@yandex.ua'
  ]

  def crawl

    _CONFIG = YAML.load_file('/home/user/sandbox/parsers/config.yml')


    system(_CONFIG['newnim'])
    sleep 1.5


    all_mails = []
    done_mails = []


    begin

#       @@driver.get "http://2ip.ru"
# exit
      @@driver.get "http://www.crush-game.com/"


    rescue
      @@driver.close
      return
    end

p 1
    begin
      session = GoogleDrive.saved_session(@@config_json)
      ws = session.file_by_title(@@google_drive_base).worksheets[3]
      (1..ws.num_rows).each do |row|
        if ws[row,7] != 'skiped'
          @@mail_domains.map { |domain| all_mails.push ws[row,1]+domain }
        end
      end
p 2
      session = GoogleDrive.saved_session(@@config_json)
      ws = session.file_by_title(@@google_drive_out).worksheets[0]
      @@done_list_max_row = ws.num_rows
      (1..ws.num_rows).each do |row|
        done_mails.push ws[row,1]
      end
p 3
    rescue
      p "Can't reach GOOGLE DRIVE!"
      return
    end
p 4
    to_crawl = all_mails - done_mails

    p 'To crawl list generated: '+to_crawl.length.to_s


to_crawl.shuffle.each do |email|

    begin

#      @@driver.get "http://www.playmxm.com/en/"
      # button = @@driver.find_element :xpath => "//*[@id='home-page']/div[1]/button/span[2]"
      # button.click


      input = @@driver.find_element :xpath => "//*[@id='mail']";
      input.send_keys(email);


      button = @@driver.find_element :xpath => "//*[@id='contact-form']/input[1]"
      button.click

      sleep 0.5

      @@driver.close
      @@headless.stop if Capybara.current_driver == :selenium

      begin
        session = GoogleDrive.saved_session(@@config_json)
        ws = session.file_by_title(@@google_drive_out).worksheets[0]
        col = ws.num_rows+1
        col = 1 if col < 1
        ws[col, 1] = email
        ws[col, 2] = Time.now
        ws.save
      rescue
        p "Can't reach GOOGLE DRIVE!"
        return
      end

    rescue
      @@driver.close if @@driver
      return
    end

    return
end

  end
end



ExampleCrawler.new.crawl


